import createAxiosInstance from '../handlers/axiosInstance';
import { endpoints } from '../handlers/endpoint';

const axiosInstance = createAxiosInstance();

export const fetchUsers = async () => {
  try {
    const response = await axiosInstance.get(endpoints.users);
    return response.data;
  } catch (error) {
    console.error('Error fetching users:', error);
    throw error;
  }
};

export const postUsers = async (user) => {
  try {
    const response = await axiosInstance.post(endpoints.users, user);
    return response.data;
  } catch (error) {
    console.error('Error posting user:', error);
    throw error;
  }
};

export const updateUser = async (userId, userData) => {
  try {
    const response = await axiosInstance.patch(`${endpoints.users}/${userId}`, userData);
    return response.data;
  } catch (error) {
    console.error('Error updating user:', error);
    throw error;
  }
};


export const deleteUser = async (userId) => {
  try {
    const response = await axiosInstance.delete(`${endpoints.users}/${userId}`);
    return response.data;
  } catch (error) {
    console.error('Error deleting user:', error);
    throw error;
  }
};

// Implement other CRUD operations similarly...
