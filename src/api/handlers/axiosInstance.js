import axios from 'axios';

const API_BASE_URL = 'https://basicbackend-j6kk.onrender.com/v1'; 

const createAxiosInstance = () => {
  return axios.create({
    baseURL: API_BASE_URL,
    timeout: 5000, 
  });
};

export default createAxiosInstance;
